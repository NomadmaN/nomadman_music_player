const nowPlaying = document.querySelector('.now-playing');
const trackArt = document.querySelector('.track-art');
const trackName = document.querySelector('.track-name');
const trackArtist = document.querySelector('.track-artist');
const playPauseTrack = document.querySelector('.playpause-track');
const nextTrack = document.querySelector('.next-track');
const prevTrack = document.querySelector('.prev-track');
const seekSlider = document.querySelector('.seek_slider');
const volumeSlider = document.querySelector('.volume_slider');
const currentTime = document.querySelector('.current-time');
const totalDuration = document.querySelector('.total-duration');
const randomTrack = document.querySelector('.random-track');
const repeatTrack = document.querySelector('.repeat-track');
const audio = document.querySelector("#audio");
const disc = document.querySelector(".disc");
const wave = document.querySelector('.wave');
const playList = document.getElementById('playlist');
const playlistToogle = document.querySelector('.playlist-toogle');
const musicDetailState = document.querySelector('.music-detail');
const controlButtonsState = document.getElementById('control-buttons');
const MUSIC_PLAYER_STORAGE = 'USER';
var updateTimer;

const musicApp = {
    // set first song as currentIndex
    currentIndex: 0,
    // initiate boolean for some toogle item
    isPlaying: false,
    isRandom: false,
    isRepeat: false,
    isPlaylist: false,
    // setting save local storage
    config: JSON.parse(localStorage.getItem(MUSIC_PLAYER_STORAGE)) || {}, 
    setConfig: function (key, value) {
        this.config[key] = value;
        localStorage.setItem(MUSIC_PLAYER_STORAGE, JSON.stringify(this.config))
    },
    // load local storage
    loadConfig: function(){
      this.isRandom = this.config.isRandom;
        if (this.config.isRandom == true){
            randomTrack.classList.replace('inactive','active')
        }else{
            randomTrack.classList.replace('active', 'inactive')
        }
        this.isRepeat = this.config.isRepeat;
        if (this.config.isRepeat == true){
            repeatTrack.classList.replace('inactive','active')
        }else{
            repeatTrack.classList.replace('active', 'inactive')
        }
    },

    // render playlist
    renderSongs: function () {
        const contentHTML = SONG_LIST.map((song, index) => {
        return `
            <div id="playlist-song" class="playlist-song ${index === this.currentIndex ? 'active' : ''}" data-index="${index}">
            <img class="playlist-thumb" src="${song.image}" alt="image">
            <div class="playlist-body">
                <h3 class="playlist-title">${song.name}</h3>
                <p class="playlist-author">${song.singer}</p>
            </div>
            <div class="playlist-option data-index="${index}">
                <i class="fab fa-itunes-note"></i>
            </div>
            </div>
            `;
        });
        playList.innerHTML = contentHTML.join('');
    },

    // define prperties
    defineProperties: function () {
        // redefined current song = currentIndex in array SONG_LIST
        Object.defineProperty(this, "currentSong", {
        get: function () {
            return SONG_LIST[this.currentIndex];
        },
        });
    },

    // handling all events
    handleEvents: function () {
        const _this = this;

        // search google "animation web api" -> https://developer.mozilla.org/en-US/docs/Web/API/Web_Animations_API
        const discAnimate = disc.animate(
            [
            { transform: "rotate(0deg)" },
            { transform: "rotate(360deg)" },
            ],
            {
            duration: 20000,
            iterations: Infinity,
            }
        );
        // pause on start
        discAnimate.pause();

        // play/pause on click
        playPauseTrack.onclick = function () {
            if (_this.isPlaying) {
                audio.pause();
              } else {
                audio.play();
              }
        };
        audio.onplay = function () {
            _this.isPlaying = true;
            wave.classList.add('loader');
            // change icon play to pause
            playPauseTrack.innerHTML = '<i class="fa fa-pause-circle fa-3x"></i>';
            discAnimate.play();
            // load volume
            setVolume();
        };
        audio.onpause = function () {
            _this.isPlaying = false;
            wave.classList.remove('loader');
            playPauseTrack.innerHTML = '<i class="fa fa-play-circle fa-3x"></i>';
            discAnimate.pause();
        };
        audio.ontimeupdate = function () {
            var seekPosition = 0;
            if (!isNaN(audio.duration)) {
                seekPosition = audio.currentTime * (100 / audio.duration);
                seekSlider.value = seekPosition;
        
                var currentMinutes = Math.floor(audio.currentTime / 60);
                var currentSeconds = Math.floor(audio.currentTime - currentMinutes * 60);
                var durationMinutes = Math.floor(audio.duration / 60);
                var durationSeconds = Math.floor(audio.duration - durationMinutes * 60);
        
                if (currentSeconds < 10) { currentSeconds = "0" + currentSeconds; }
                if (durationSeconds < 10) { durationSeconds = "0" + durationSeconds; }
                if (currentMinutes < 10) { currentMinutes = "0" + currentMinutes; }
                if (durationMinutes < 10) { durationMinutes = "0" + durationMinutes; }
        
                currentTime.textContent = currentMinutes + ":" + currentSeconds;
                totalDuration.textContent = durationMinutes + ":" + durationSeconds;
            }
        };
        // update seeker when changed
        seekSlider.onchange = function (event) {
            const seekedTime = (event.target.value * audio.duration) / 100;
            audio.currentTime = seekedTime;
        };
        setVolume = function () {
          // update volume when changed
          volumeSlider.onchange = function (event) {
            const seekedVolume = (event.target.value) / 100;
            audio.volume = seekedVolume;
          };
        };
        reset = function () {
            currentTime.textContent = "00:00";
            totalDuration.textContent = "00:00";
            seekSlider.value = 0;
            volumeSlider.value = 50;
        };
        // next button
        nextTrack.onclick = function () {
            if (_this.isRandom == true){
              _this.playRandomSong();
            }else{
              _this.playNextSong();
            }
            audio.play();
            // render to update song with active
            _this.renderSongs();
            // scroll to actived song in case of long list
            _this.scrollToActiveSong();
      
        };
        prevTrack.onclick = function () {
            if (_this.isRandom == true){
              _this.playRandomSong();
            }else{
              _this.playPreviousSong();
            }
            audio.play();
            _this.renderSongs();
            _this.scrollToActiveSong();
        };
        randomTrack.onclick = function (e) {         
            if (_this.isRandom = !_this.isRandom){
                _this.isRandom == true;
                randomTrack.classList.replace('inactive','active')
                _this.setConfig('isRandom', _this.isRandom)
              }else{
                _this.isRandom == false;
                randomTrack.classList.replace('active', 'inactive')
                _this.setConfig('isRandom', _this.isRandom)
              }
        };
        repeatTrack.onclick = function (e) {
            if (_this.isRepeat = !_this.isRepeat){
              _this.isRepeat == true;
              repeatTrack.classList.replace('inactive','active')
              _this.setConfig('isRepeat', _this.isRepeat)
            }else{
              _this.isRepeat == false;
              repeatTrack.classList.replace('active', 'inactive')
              _this.setConfig('isRepeat', _this.isRepeat)
            }
        };
        playlistToogle.onclick = function (e) {         
          if (_this.isPlaylist = !_this.isPlaylist){
              _this.isPlaylist == true;
              playlistToogle.classList.replace('inactive','active')
              playList.classList.replace('normal-state','playlist-state')
              musicDetailState.classList.replace('normal-state','playlist-state')
              controlButtonsState.classList.replace('normal-state','playlist-state')
            }else{
              _this.isPlaylist == false;
              playlistToogle.classList.replace('active', 'inactive')
              playList.classList.replace('playlist-state','normal-state')
              musicDetailState.classList.replace('playlist-state','normal-state')
              controlButtonsState.classList.replace('playlist-state','normal-state')
            }
      };
        audio.onended = function(){
            if (_this.isRepeat == true){
              audio.play();
            }else{
              nextTrack.click();
            }
        };
        playList.onclick = function(e){
          const songNode = e.target.closest('.playlist-song:not(.active)');
          const songNodeOpt = e.target.closest('.playlist-option');
            if (
              // if click on song not actived or not option button, load that song by loading data-index contain ID of that song (created above)
              songNode || !songNodeOpt
              ){
                if (songNode){
                  // code bên trên không hoạt động cho active vì: currentIndex ban đầu là số, nhưng khi get element ra "songNode.dataset.index;" thì nó là chuỗi, nên phải trả ra giá trị number lại thì mới hoạt động class active lại dc (.song:active) dựa theo data-index: number
                  _this.currentIndex = Number(songNode.dataset.index);
                  _this.loadCurrentSong();
                  _this.renderSongs();
                  audio.play();
                } 
                if (songNodeOpt){
                  console.log('Add some functions for this option button!');
                  // Do it later
                }
              }
        };
    },
    

    loadCurrentSong: function () {
        clearInterval(updateTimer);
        reset();
        // render to UI
        nowPlaying.textContent = "Playing song " + (this.currentIndex + 1) + " of " + SONG_LIST.length;
        trackName.textContent = this.currentSong.name;
        trackArtist.textContent = this.currentSong.singer;
        trackArt.style.backgroundImage = `url('${this.currentSong.image}')`;
        audio.src = this.currentSong.path;
        updateTimer = setInterval(this.ontimeupdate, 1000);
    },
    playNextSong: function () {
        this.currentIndex++;
        // console.log(this.currentIndex);
        if (this.currentIndex >= SONG_LIST.length) {
          this.currentIndex = 0;
        }
        this.loadCurrentSong();
    },
    playPreviousSong: function () {
        this.currentIndex--;
        if (this.currentIndex < 0) {
          this.currentIndex = SONG_LIST.length - 1;
        }
        this.loadCurrentSong();
    },
    playRandomSong: function () {
        let randomIndex;
        do {
          randomIndex = Math.floor(Math.random() * SONG_LIST.length);
        } 
        while (
          randomIndex === this.currentIndex)
    
        this.currentIndex = randomIndex;
        this.loadCurrentSong();
    },
    scrollToActiveSong: function () {
        // pause 500ms before move to song
        setTimeout(() => {
          // to move to an element, search "scrollIntoView element" -> 
          document.querySelector('.playlist-song.active').scrollIntoView({
            block: 'nearest', 
            behavior: 'smooth' })
        }, 500)
    },

    // start function store all function need to start
    start: function () {
        this.defineProperties();
        this.handleEvents();
        this.loadCurrentSong();
        this.renderSongs();
         // load local storage
        this.loadConfig();  
        randomBackgroundColor();
    },

};

// color setting
function randomBackgroundColor() {
    let colorOne = populate();
    let colorTwo = populate();
    document.body.style.background = 'linear-gradient(to right,' + colorOne + ',' + colorTwo + ")";
    document.querySelector(".seek_slider").style.background = 'linear-gradient(to right,' + colorOne + ',' + colorTwo + ")";
    document.querySelector(".volume_slider").style.background = 'linear-gradient(to right,' + colorOne + ',' + colorTwo + ")";
    document.querySelector("#playlist").style.background = 'linear-gradient(to right,' + colorOne + ',' + colorTwo + ")";
    // document.getElementById("playlist-song").style.background = 'linear-gradient(to right,' + colorOne + ',' + colorTwo + ")";
};

function populate() {
    let hex = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e'];
    let colorRandom = "#";
    for (var i = 0; i < 6; i++) {
        var x = Math.round(Math.random() * 14);
        var y = hex[x];
        colorRandom += y;
    }
    return colorRandom;
};

// initially only run 1 function start
musicApp.start();